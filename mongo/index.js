const MongoClient = require('mongodb').MongoClient;

exports.instance = async (url) => {
    const client = await MongoClient.connect(url, { useNewUrlParser: true });
    const db = client.db();
    return { client, db };
}

module.exports = exports;
